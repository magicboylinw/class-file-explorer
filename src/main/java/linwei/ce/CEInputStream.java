/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:CEInputStream
 * @Author:林威
 * @CreateDate:2015年4月13日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce;

import java.io.IOException;
import java.io.InputStream;

 /** 
 * @ClassName: CEInputStream
 * @Description: 
 * @author: 林威
 * @date: 2015年4月13日
 * 
 */
public class CEInputStream extends InputStream {
	
	private final InputStream inner;
	
	public CEInputStream(InputStream inner){
		this.inner = inner;
	}

	@Override
	public int read() throws IOException {
		return inner.read();
	}
	
	/**
	 * 一次读length个字节
	 * @param length
	 * @return
	 * @throws IOException
	 */
	public byte[] read(int length) throws IOException{
		byte[] val = new byte[length];
		read(val);
		return val;
	}
	
}
