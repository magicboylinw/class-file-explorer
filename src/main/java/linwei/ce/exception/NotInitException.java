/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:NotInitException
 * @Author:林威
 * @CreateDate:2015年4月22日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.exception;

 /** 
 * @ClassName: NotInitException
 * @Description: 
 * @author: 林威
 * @date: 2015年4月22日
 * 
 */
public class NotInitException extends Exception {

	/**
	 * 
	 */
	public NotInitException() {
	}

	/**
	 * @param message
	 */
	public NotInitException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public NotInitException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public NotInitException(String message, Throwable cause) {
		super(message, cause);
	}

}
