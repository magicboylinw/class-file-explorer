/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:AccessFlags
 * @Author:林威
 * @CreateDate:2015年4月22日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type;

import linwei.ce.Utils;

 /** 
 * @ClassName: AccessFlags
 * @Description: 
 * @author: 林威
 * @date: 2015年4月22日
 * 
 */
public class AccessFlags extends BaseType {
	
	public static final int ACC_PUBLIC = 0x0001;
	public static final int ACC_FINAL = 0x0010;
	public static final int ACC_INTERFACE = 0x0200;
	public static final int ACC_ABSTRACT = 0x0400;

	/**
	 * 接口或类的访问标识，掩码表示，也就是：FLAG1 | FLAG2 | FLAG3 | ...
	 * 4 bytes
	 * @param bytes
	 */
	public AccessFlags(byte[] bytes) {
		super(bytes);
	}
	
	/**
	 * public?
	 * @return
	 */
	public boolean isPublic(){
		return (Utils.toInt(bytes) & 0x000F) == ACC_PUBLIC;
	}
	
	/**
	 * final?
	 * @return
	 */
	public boolean isFinal(){
		return (Utils.toInt(bytes) & 0x00F0) == ACC_FINAL;
	}
	/**
	 * interface?
	 * @return
	 */
	public boolean isInterface(){
		return (Utils.toInt(bytes) & 0x0F00) == ACC_INTERFACE;
	}
	/**
	 * abstract?
	 * @return
	 */
	public boolean isAbstract(){
		return (Utils.toInt(bytes) & 0x0F00) == ACC_ABSTRACT;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		if (isPublic()) {
			builder.append("public ");
		} else if (isFinal()) {
			builder.append("final ");
		} else if (isAbstract()) {
			builder.append("abstract ");
		} else if (isInterface()) {
			builder.append("interface ");
		}
		return builder.toString();
	}
}
