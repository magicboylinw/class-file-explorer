/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:MinorVersion
 * @Author:林威
 * @CreateDate:2015年4月13日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type;

import linwei.ce.Utils;

 /** 
 * @ClassName: MinorVersion
 * @Description: 次版本号
 * @author: 林威
 * @date: 2015年4月13日
 * 
 */
public class MinorVersion extends BaseType {

	/**
	 * 2 bytes
	 * @param orignByte
	 */
	public MinorVersion(byte[] orignByte) {
		super(orignByte);
	}
	
	public int toInt(){
		return Utils.toInt(bytes);
	}

}
