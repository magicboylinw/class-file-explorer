/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:FieldInfo
 * @Author:林威
 * @CreateDate:2015年4月23日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type;

import linwei.ce.Utils;


 /** 
 * @ClassName: FieldInfo
 * @Description: 
 * @author: 林威
 * @date: 2015年4月23日
 * 
 */
public class FieldInfo extends BaseType {
	
	/* 字段的修饰符常量  */
	public static final int ACC_PUBLIC = 0x0001;
	public static final int ACC_PRIVATE = 0x0002;
	public static final int ACC_PROTECTED = 0x0004;
	public static final int ACC_STATIC = 0x0008;
	public static final int ACC_FINAL = 0x0010;
	public static final int ACC_VOLATILE = 0x0040;
	public static final int ACC_TRANSIENT = 0x0080;
	public static final int ACC_SYNTHETIC = 0x1000;		// 表示字段是由编译器生成的
	public static final int ACC_ENUM = 0x4000;
	
	/* 字段的类型描述符，右边是描述符，左边是描述符代表的数据类型 */
	public static final char DESC_BYTE = 'B';
	public static final char DESC_CHAR = 'C';
	public static final char DESC_DOUBLE = 'D';
	public static final char DESC_FLOAT = 'F';
	public static final char DESC_INT = 'I';
	public static final char DESC_LONG = 'J';
	public static final char DESC_SHORT = 'S';
	public static final char DESC_BOOLEAN = 'Z';
	public static final char DESC_INSTANCE = 'L';	// Ljava/lang/String 表示它是一个java.lang.String的实例
	public static final char DESC_ARRAY = '[';		// '['表示一维数组，'[['表示二维数组，以此类推

	
	
	/**
	 * 代表字段（类或实例）
	 * accessFlags - 2 bytes, 字段的修饰符
	 * nameIndex - 2 bytes, 字段的名称（在常量池中的索引），指向UTF8类型
	 * descriptorIndex - 2 bytes, 字段类型的描述符（常量池索引），用以描述字段的类型
	 * attributesCount - 2 bytes, 域的附加属性数量
	 * attribut[attributesCount] - 域的附加属性列表
	 * 
	 * @param bytes
	 */
	public FieldInfo(byte[] bytes) {
		super(bytes);
	}
	
	/* 判断字段修饰符的操作  */
	public boolean isPublic(){
		return (Utils.toInt(bytes, 0, 2) & 0x000F) == ACC_PUBLIC;
	}
	public boolean isPrivate(){
		return (Utils.toInt(bytes, 0, 2) & 0x000F) == ACC_PRIVATE;
	}
	public boolean isProtected(){
		return (Utils.toInt(bytes, 0, 2) & 0x000F) == ACC_PROTECTED;
	}
	public boolean isStatic(){
		return (Utils.toInt(bytes, 0, 2) & 0x000F) == ACC_STATIC;
	}
	public boolean isFinal(){
		return (Utils.toInt(bytes, 0, 2) & 0x00F0) == ACC_FINAL;
	}
	public boolean isVolatile(){
		return (Utils.toInt(bytes, 0, 2) & 0x00F0) == ACC_VOLATILE;
	}
	public boolean isTransient(){
		return (Utils.toInt(bytes, 0, 2) & 0x00F0) == ACC_TRANSIENT;
	}
	public boolean isSynthetic(){
		return (Utils.toInt(bytes, 0, 2) & 0x0F00) == ACC_SYNTHETIC;
	}
	public boolean isEnum(){
		return (Utils.toInt(bytes, 0, 2) & 0x0F00) == ACC_ENUM;
	}
	
	/**
	 * 返回字段的名称
	 * @return
	 */
	public String getName(){
		
	}
	
	/**
	 * 返回字段的类型描述符
	 * @return
	 */
	public String getDescriptor(){
		
	}

}
