/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:Base
 * @Author:林威
 * @CreateDate:2015年4月10日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type;

import java.util.Arrays;


 /** 
 * @ClassName: Base
 * @Description: 所有数据结构的基类
 * @author: 林威
 * @date: 2015年4月10日
 * 
 */
public abstract class BaseType {
	/**
	 * 保存原始的字节数据
	 */
	protected byte[] bytes;

	public BaseType(byte[] bytes) {
		this.bytes = Arrays.copyOf(bytes, bytes.length);
	}

}
