/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:ConstantPoolCount
 * @Author:林威
 * @CreateDate:2015年4月13日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type;

import linwei.ce.Utils;

 /** 
 * @ClassName: ConstantPoolCount
 * @Description: 常量池的数量，实际上常量池中的条目会比这个数量少一条
 * @author: 林威
 * @date: 2015年4月13日
 * 
 */
public class ConstantPoolCount extends BaseType{

	/**
	 * 常量池的数量
	 * 2 bytes
	 * @param bytes
	 */
	public ConstantPoolCount(byte[] bytes) {
		super(bytes);
	}
	
	public int toInt(){
		return Utils.toInt(bytes);
	}

}
