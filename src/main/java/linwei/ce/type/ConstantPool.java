/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:ConstantPool
 * @Author:林威
 * @CreateDate:2015年4月22日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type;

import java.util.ArrayList;

import linwei.ce.type.constantpool.BaseConstant;

 /** 
 * @ClassName: ConstantPool
 * @Description: 常量池，第一个元素为null，常量池索引始终从1开始
 * @author: 林威
 * @date: 2015年4月22日
 * 
 */
public class ConstantPool extends ArrayList<BaseConstant> {

	public ConstantPool(int initialCapacity) {
		super(initialCapacity);
		add(null);
	}

}
