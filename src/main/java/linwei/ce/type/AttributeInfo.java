/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:AttributeInfo
 * @Author:林威
 * @CreateDate:2015年4月23日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type;

 /** 
 * @ClassName: AttributeInfo
 * @Description: 
 * @author: 林威
 * @date: 2015年4月23日
 * 
 */
public class AttributeInfo extends BaseType {

	/**
	 * 字段的附加属性
	 * @param bytes
	 */
	public AttributeInfo(byte[] bytes) {
		super(bytes);
	}

}
