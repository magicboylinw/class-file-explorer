/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:MagicNumber
 * @Author:林威
 * @CreateDate:2015年4月10日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type;

 /** 
 * @ClassName: MagicNumber
 * @Description: java class 文件的魔数
 * @author: 林威
 * @date: 2015年4月10日
 * 
 */
public class MagicNumber extends BaseType {
	
	/**
	 * 魔数，十六进制
	 */
	public static final int mn = 0xCAFEBABE;

	/**
	 * 4 bytes
	 * @param bytes
	 */
	public MagicNumber(byte[] bytes) {
		super(bytes);
	}
	
	public String toHexString(){
		int intVal = ((int) bytes[0]) << 24 
				| (((int) bytes[1] << 16) & 0x00FF0000) 
				| (((int) bytes[2] << 8) & 0x0000FF00) 
				| ((int) bytes[3] & 0x000000FF);
		return Integer.toHexString(intVal);
	}
}
