/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:InvokeDynamic
 * @Author:林威
 * @CreateDate:2015年4月20日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type.constantpool;

import linwei.ce.Utils;
import linwei.ce.exception.NotInitException;


 /** 
 * @ClassName: InvokeDynamic
 * @Description: 
 * @author: 林威
 * @date: 2015年4月20日
 * 
 */
public class InvokeDynamic extends BaseConstant {
	
	public static final byte tag = (byte) 18;
	
	/**
	 * 动态代理？
	 * tag - 1 byte, 18
	 * bootstrapMethodAttrIndex - 1 byte
	 * nameAndType - 2 bytes, 指向NameAndType的索引
	 * @param bytes
	 */
	public InvokeDynamic(byte[] bytes) {
		super(bytes);
	}

	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTag()
	 */
	@Override
	public byte getTag() {
		return tag;
	}

	@Override
	public String toString() {
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("bootstrapMethodAttrIndex ").append(Utils.toInt(bytes, 1, 1)).append("\n");
			sb.append(getConstant(Utils.toInt(bytes, 2, 2)));
			return sb.toString();
		} catch (NotInitException e) {
			return e.getMessage();
		}
	}

	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTypeDesc()
	 */
	@Override
	public String getTypeDesc() {
		return "InvokeDynamic";
	}
	
}
