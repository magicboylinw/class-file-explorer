/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:NameAndType
 * @Author:林威
 * @CreateDate:2015年4月20日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type.constantpool;

import linwei.ce.Utils;
import linwei.ce.exception.NotInitException;


 /** 
 * @ClassName: NameAndType
 * @Description: 
 * @author: 林威
 * @date: 2015年4月20日
 * 
 */
public class NameAndType extends BaseConstant {
	
	public static final byte tag = (byte) 12;
	
	/**
	 * 属性或方法的名称和类型对
	 * tag - 1 byte, 12
	 * nameIndex - 2 bytes, 指向UTF8类型的索引，表示名称
	 * descriptorIndex - 2 bytes, 指向UTF8类型的索引，表示类型描述符
	 * @param bytes
	 */
	public NameAndType(byte[] bytes) {
		super(bytes);
	}
	
	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTag()
	 */
	@Override
	public byte getTag() {
		return tag;
	}

	@Override
	public String toString() {
		try {
			StringBuilder builder = new StringBuilder();
			builder.append(getConstant(Utils.toInt(bytes, 1, 2)).toString()).append(":");
			builder.append(getConstant(Utils.toInt(bytes, 3, 2)).toString());
			return builder.toString();
		} catch (NotInitException e) {
			return e.getMessage();
		}
	}

	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTypeDesc()
	 */
	@Override
	public String getTypeDesc() {
		return "NameAndType";
	}
	
	/**
	 * 返回名称（name）
	 * @return
	 */
	public String getName(){
		try {
			return getConstant(Utils.toInt(bytes, 1, 2)).toString();
		} catch (NotInitException e) {
			return e.getMessage();
		}
	}
	
	/**
	 * 返回类型（type）
	 * @return
	 */
	public String getType(){
		try {
			return getConstant(Utils.toInt(bytes, 3, 2)).toString();
		} catch (NotInitException e) {
			return e.getMessage();
		}
	}
}
