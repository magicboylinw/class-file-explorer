/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:FloatInfo
 * @Author:林威
 * @CreateDate:2015年4月20日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type.constantpool;

import linwei.ce.Utils;

 /** 
 * @ClassName: FloatInfo
 * @Description: 
 * @author: 林威
 * @date: 2015年4月20日
 * 
 */
public class FloatInfo extends BaseConstant {
	
	public static final byte tag = (byte) 4;
	
	/**
	 * 代表float
	 * tag - 1 byte, 4
	 * float - 4 bytes
	 * @param bytes
	 */
	public FloatInfo(byte[] bytes) {
		super(bytes);
	}

	@Override
	public byte getTag() {
		return tag;
	}

	@Override
	public String toString() {
		return String.valueOf(toFloat());
	}
	
	public float toFloat(){
		return Float.intBitsToFloat(Utils.toInt(bytes, 1, 4));
	}

	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTypeDesc()
	 */
	@Override
	public String getTypeDesc() {
		return "Float";
	}

}
