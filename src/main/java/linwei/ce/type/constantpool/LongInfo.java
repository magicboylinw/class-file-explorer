/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:LongInfo
 * @Author:林威
 * @CreateDate:2015年4月20日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type.constantpool;

import linwei.ce.Utils;


 /** 
 * @ClassName: LongInfo
 * @Description: 
 * @author: 林威
 * @date: 2015年4月20日
 * 
 */
public class LongInfo extends BaseConstant {
	
	public static final byte tag = (byte) 5;
	
	/**
	 * 表示Long常量
	 * tag - 1 byte
	 * high - 高4个字节
	 * low  - 低4个字节
	 * @param bytes
	 */
	public LongInfo(byte[] bytes) {
		super(bytes);
	}
	
	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTag()
	 */
	@Override
	public byte getTag() {
		return tag;
	}
	
	public long toLong(){
		return Utils.toLong(bytes, 1, 8);
	}

	@Override
	public String toString() {
		return String.valueOf(toLong());
	}

	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTypeDesc()
	 */
	@Override
	public String getTypeDesc() {
		return "Long";
	}
	
}
