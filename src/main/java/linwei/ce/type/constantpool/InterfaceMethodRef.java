/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:InterfaceMethodRef
 * @Author:林威
 * @CreateDate:2015年4月18日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type.constantpool;

import linwei.ce.Utils;
import linwei.ce.exception.NotInitException;


 /** 
 * @ClassName: InterfaceMethodRef
 * @Description: 
 * @author: 林威
 * @date: 2015年4月18日
 * 
 */
public class InterfaceMethodRef extends BaseConstant {
	
	public static final byte tag = (byte) 11;
	
	/**
	 * 接口方法类型
	 * tag - 1 byte, 11
	 * classIndex - 2 bytes, 指向ClassInfo常量的索引
	 * nameAndType - 2 bytes, 指向NameAndType常量的索引
	 * @param bytes
	 */
	public InterfaceMethodRef(byte[] bytes) {
		super(bytes);
	}

	@Override
	public byte getTag() {
		return tag;
	}
	
	@Override
	public String toString() {
		try {
			ClassInfo classInfo = (ClassInfo) getConstant(Utils.toInt(bytes, 1, 2));
			NameAndType nameAndType = (NameAndType) getConstant(Utils.toInt(bytes, 3, 2));
			return classInfo.toString() + ", " + nameAndType.toString();
		} catch (NotInitException e) {
			return e.getMessage();
		}
	}

	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTypeDesc()
	 */
	@Override
	public String getTypeDesc() {
		return "InterfaceMethodRef";
	}

}
