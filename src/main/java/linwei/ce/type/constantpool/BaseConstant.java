/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:BaseConstant
 * @Author:林威
 * @CreateDate:2015年4月20日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type.constantpool;

import java.text.MessageFormat;

import linwei.ce.exception.NotInitException;
import linwei.ce.type.BaseType;
import linwei.ce.type.ConstantPool;

 /** 
 * @ClassName: BaseConstant
 * @Description: 
 * @author: 林威
 * @date: 2015年4月20日
 * 
 */
public abstract class BaseConstant extends BaseType {

	/**
	 * 关联的常量池
	 * 这样每个常量就可以查找其他常量
	 */
	private ConstantPool constantPool;
	
	/**
	 * 返回常量类型的tag类型
	 * @return
	 */
	public abstract byte getTag();
	
	/**
	 * 返回常量类型的描述
	 * @return
	 */
	public abstract String getTypeDesc();
	
	/**
	 * 从常量池里取得某一常量
	 * @param index 常量池索引
	 * @return
	 */
	public BaseConstant getConstant(int index) throws NotInitException{
		try {
			return constantPool.get(index);
		} catch (IndexOutOfBoundsException e) {
			throw new NotInitException(MessageFormat.format("[constant #{0} not init yet]", index), e);
		}
	}

	public BaseConstant(byte[] bytes) {
		super(bytes);
	}
	public ConstantPool getConstantPool() {
		return constantPool;
	}

	public void setConstantPool(ConstantPool constantPool) {
		this.constantPool = constantPool;
	}
}
