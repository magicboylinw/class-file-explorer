/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:UTF8Info
 * @Author:林威
 * @CreateDate:2015年4月20日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type.constantpool;

import java.io.UnsupportedEncodingException;

import linwei.ce.Utils;

 /** 
 * @ClassName: UTF8Info
 * @Description: 
 * @author: 林威
 * @date: 2015年4月20日
 * 
 */
public class UTF8Info extends BaseConstant {
	
	public static final byte tag = (byte) 1;
	
	/**
	 * UTF8格式存储的字符串
	 * tag - 1 byte, 1
	 * length - 2 bytes, 字节数组的长度
	 * bytes - length bytes, 字节数组
	 * @param bytes
	 */
	public UTF8Info(byte[] bytes) {
		super(bytes);
	}

	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTag()
	 */
	@Override
	public byte getTag() {
		return tag;
	}

	@Override
	public String toString() {
		try {
			return new String(bytes, 3, Utils.toInt(bytes, 1, 2), "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTypeDesc()
	 */
	@Override
	public String getTypeDesc() {
		return "UTF8";
	}
}
