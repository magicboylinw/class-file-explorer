/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:IntegerInfo
 * @Author:林威
 * @CreateDate:2015年4月20日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type.constantpool;

import linwei.ce.Utils;


 /** 
 * @ClassName: IntegerInfo
 * @Description: 
 * @author: 林威
 * @date: 2015年4月20日
 * 
 */
public class IntegerInfo extends BaseConstant {
	
	public static final byte tag = (byte) 3;
	
	/**
	 * 代表integer
	 * tag - 1 byte, 3
	 * integer - 4 bytes
	 * @param bytes
	 */
	public IntegerInfo(byte[] bytes) {
		super(bytes);
	}


	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTag()
	 */
	@Override
	public byte getTag() {
		return tag;
	}
	
	public int toInt(){
		return Utils.toInt(bytes, 1, 4);
	}

	@Override
	public String toString() {
		return String.valueOf(toInt());
	}


	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTypeDesc()
	 */
	@Override
	public String getTypeDesc() {
		return "Integer";
	}
	
}
