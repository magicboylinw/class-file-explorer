/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:MethodType
 * @Author:林威
 * @CreateDate:2015年4月20日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type.constantpool;

import linwei.ce.Utils;
import linwei.ce.exception.NotInitException;


 /** 
 * @ClassName: MethodType
 * @Description: 
 * @author: 林威
 * @date: 2015年4月20日
 * 
 */
public class MethodType extends BaseConstant {
	
	public static final byte tag = (byte) 16;
	
	/**
	 * 方法描述符
	 * tag - 1 byte, 16
	 * descriptorIndex - 2 bytes, 指向UTF8类型的索引，表示方法描述符
	 * @param bytes
	 */
	public MethodType(byte[] bytes) {
		super(bytes);
	}
	
	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTag()
	 */
	@Override
	public byte getTag() {
		return tag;
	}

	@Override
	public String toString() {
		try {
			StringBuilder builder = new StringBuilder();
			builder.append(getConstant(Utils.toInt(bytes, 1, 2)).toString());
			return builder.toString();
		} catch (NotInitException e) {
			return e.getMessage();
		}
	}

	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTypeDesc()
	 */
	@Override
	public String getTypeDesc() {
		return "MethodType";
	}
}
