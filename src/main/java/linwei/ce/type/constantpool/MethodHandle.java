/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:MethodHandle
 * @Author:林威
 * @CreateDate:2015年4月20日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type.constantpool;

import linwei.ce.Utils;


 /** 
 * @ClassName: MethodHandle
 * @Description: 
 * @author: 林威
 * @date: 2015年4月20日
 * 
 */
public class MethodHandle extends BaseConstant {
	
	public static final byte tag = (byte) 15;
	
	/**
	 * 方法处理器
	 * tag - 1 byte, 15
	 * referenceKind - 1 byte
	 * referenceIndex - 2 bytes
	 * @param bytes
	 */
	public MethodHandle(byte[] bytes) {
		super(bytes);
	}
	
	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTag()
	 */
	@Override
	public byte getTag() {
		return tag;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("referenceKind ").append(Utils.toInt(bytes, 1, 1)).append("\n");
		builder.append("referenceIndex ").append(Utils.toInt(bytes, 2, 2));
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTypeDesc()
	 */
	@Override
	public String getTypeDesc() {
		return "MethodHandle";
	}
	
}
