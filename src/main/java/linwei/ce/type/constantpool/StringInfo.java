/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:StringInfo
 * @Author:林威
 * @CreateDate:2015年4月18日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type.constantpool;

import linwei.ce.Utils;
import linwei.ce.exception.NotInitException;


 /** 
 * @ClassName: StringInfo
 * @Description: 
 * @author: 林威
 * @date: 2015年4月18日
 * 
 */
public class StringInfo extends BaseConstant {
	
	public static final byte tag = (byte) 8;
	
	/**
	 * 字符串常量
	 * tag - 1 byte, 8
	 * stringIndex - 2 bytes, 指向UTF8类型的索引，字符串的内容
	 * @param bytes
	 */
	public StringInfo(byte[] bytes) {
		super(bytes);
	}
	
	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTag()
	 */
	@Override
	public byte getTag() {
		return tag;
	}

	@Override
	public String toString() {
		try {
			StringBuilder builder = new StringBuilder();
			builder.append(getConstant(Utils.toInt(bytes, 1, 2)).toString());
			return builder.toString();
		} catch (NotInitException e) {
			return e.getMessage();
		}
	}

	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTypeDesc()
	 */
	@Override
	public String getTypeDesc() {
		return "String";
	}
	
}
