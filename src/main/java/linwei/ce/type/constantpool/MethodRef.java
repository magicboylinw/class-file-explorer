/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:MethodRef
 * @Author:林威
 * @CreateDate:2015年4月18日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type.constantpool;

import linwei.ce.Utils;
import linwei.ce.exception.NotInitException;


 /** 
 * @ClassName: MethodRef
 * @Description: 
 * @author: 林威
 * @date: 2015年4月18日
 * 
 */
public class MethodRef extends BaseConstant {
	
	public static final byte tag = (byte) 10;
	
	/**
	 * 对方法的引用
	 * tag - 1 byte, 10
	 * classIndex - 2 bytes, 属性所在类的名称的索引，指向ClassInfo类型
	 * nameAndTypeIndex - 2 bytes，属性名称和属性类型的索引，指向NameAndType类型
	 * @param bytes
	 */
	public MethodRef(byte[] bytes) {
		super(bytes);
	}

	@Override
	public byte getTag() {
		return tag;
	}
	
	@Override
	public String toString() {
		try {
			ClassInfo classInfo = (ClassInfo) getConstant(Utils.toInt(bytes, 1, 2));
			NameAndType nameAndType = (NameAndType) getConstant(Utils.toInt(bytes, 3, 2));
			return classInfo.toString() + "." + nameAndType.getName() + ":" + nameAndType.getType();
		} catch (NotInitException e) {
			return e.getMessage();
		}
	}

	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTypeDesc()
	 */
	@Override
	public String getTypeDesc() {
		return "MethodRef";
	}

}
