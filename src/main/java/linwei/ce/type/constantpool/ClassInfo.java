/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:ClassInfo
 * @Author:林威
 * @CreateDate:2015年4月18日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type.constantpool;

import linwei.ce.Utils;
import linwei.ce.exception.NotInitException;


 /** 
 * @ClassName: ClassInfo
 * @Description: 类名的字符串
 * @author: 林威
 * @date: 2015年4月18日
 * 
 */
public class ClassInfo extends BaseConstant {
	
	public static final byte tag = (byte) 7;
	
	/**
	 * tag - 1 byte
	 * nameIndex - 2 bytes, 类名字符串的常量池索引，指向UTF8
	 * @param bytes
	 */
	public ClassInfo(byte[] bytes) {
		super(bytes);
	}

	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTag()
	 */
	@Override
	public byte getTag() {
		return tag;
	}

	@Override
	public String toString() {
		try {
			UTF8Info utf8 = (UTF8Info) getConstant(Utils.toInt(bytes, 1, 2));
			return utf8.toString();
		} catch (NotInitException e) {
			return e.getMessage();
		}
	}

	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTypeDesc()
	 */
	@Override
	public String getTypeDesc() {
		return "Class";
	}
	
}
