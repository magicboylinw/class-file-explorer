/** 
 * @Title:
 * @Desription:
 * @Company:CSN
 * @ClassName:DoubleInfo
 * @Author:林威
 * @CreateDate:2015年4月20日
 * @UpdateUser:林威
 * @Version:0.1
 *    
 */ 
package linwei.ce.type.constantpool;

import linwei.ce.Utils;


 /** 
 * @ClassName: DoubleInfo
 * @Description: 
 * @author: 林威
 * @date: 2015年4月20日
 * 
 */
public class DoubleInfo extends BaseConstant {
	
	public static final byte tag = (byte) 6;
	
	/**
	 * 表示Double常量
	 * tag - 1 byte
	 * high - 高4个字节
	 * low  - 低4个字节
	 * @param bytes
	 */
	public DoubleInfo(byte[] bytes) {
		super(bytes);
	}

	@Override
	public byte getTag() {
		return tag;
	}

	@Override
	public String toString() {
		return String.valueOf(toDouble());
	}
	
	public double toDouble(){
		return Double.longBitsToDouble(Utils.toLong(bytes, 1, 8));
	}

	/* (non-Javadoc)
	 * @see linwei.ce.type.constantpool.BaseConstant#getTypeDesc()
	 */
	@Override
	public String getTypeDesc() {
		return "Double";
	}
	
}
